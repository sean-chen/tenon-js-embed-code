<?php
// keep going even if user bounces off the page
ignore_user_abort(true);

// Tenon should never take any longer than 30 seconds
// in most cases responses are less than 5 seconds
set_time_limit(30);

require('config.php');
require('tenonTest.class.php');

// this section basically creates the $opts array from the $_POST data
// it only sets the items that are non-blank. This allows Tenon to revert to defaults
$expectedPost = array('src', 'level', 'certainty', 'priority',
    'docID', 'systemID', 'reportID', 'viewPortHeight', 'viewPortWidth',
    'uaString', 'importance', 'ref', 'importance', 'fragment', 'store');

foreach ($_POST AS $k => $v) {
    if (in_array($k, $expectedPost)) {
        if (strlen(trim($v)) > 0) {
            $opts[$k] = $v;
        }
    }
}

$opts['key'] = TENON_API_KEY;

$tenon = new tenonTest(TENON_API_URL, $opts);

if (false === $tenon->hashExists()) {
    $tenon->tURL = $_POST['tURL'];
    $tenon->submit(DEBUG);

    //return tenon's response code
    http_response_code($tenon->tCode);

    echo $tenon->tCode;
    $tenon->writeResultsToCSV(CSV_FILE_PATH, CSV_FILE_NAME, CSV_FILE_MODE);
}

// explicitly bail
else{
    exit;
}